const express = require('express');
const app = express();

const hbs = require('hbs');
require('./hbs/helpers');

const port = process.env.PORT || 8080


app.use(express.static(__dirname + '/public'));

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');


app.get('/', (req, res) => {

    res.render('home', {
        nombre: 'joshua',
    });

});

app.get('/about', (req, res) => {

    res.render('about');

});

app.get('/data', (req, res) => {

    res.send('Hola Data');


    res.send(salida);

});

app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${port}`);

});